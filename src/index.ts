/**
 * This part is for testing performance of any application using our tracking script
 */

import {
  browserOptions,
  Config,
  getTrackingScript
} from './shared';
import * as puppeteer from 'puppeteer';
import { Page } from 'puppeteer';
import { BrowserEventObj } from 'puppeteer';

declare global {
  interface Window {
    _cux: any;
    _cuxSettings: any;
  }
}

export class Artemis {
  private config: Config;

  constructor(config: Config) {
    this.config = config;
  }

  async openBrowser() {
    const browser = await puppeteer.launch(browserOptions);
    const pages = await browser.pages();
    const page = pages[0];

    await page.setDefaultNavigationTimeout(0);
    await page.goto(this.config.project.url, { waitUntil: 'networkidle0' });

    await this.injectTrackingScript(page);

    browser.on('targetchanged', (target) => this.onTargetChanged(target, browser));
  }

  async onTargetChanged(target: BrowserEventObj['targetchanged'], browser: any) {
    const pages = await browser.pages();
    const page = pages[0];
    await page.waitForNavigation();
    await this.injectTrackingScript(page);
  }

  async injectTrackingScript(page: Page) {
    /**
     * If website you are testing currently have tracking script,
     * uncomment below so we will firstly remove production script
     * and inject local script
     */
    // await page.evaluate(() => {
    //   Array.from(document.querySelectorAll('script'))
    //     .filter(node => node.text.includes('cux') || node.src.includes('dc.cux'))
    //     .forEach(node => node.remove());
    //
    //   window._cux = undefined;
    //   window._cuxSettings = undefined;
    //
    //   localStorage.removeItem('_cux_u');
    //   localStorage.removeItem('_cux_v_ttl');
    //   localStorage.removeItem('_cux_v');
    //
    //   sessionStorage.removeItem('_cux_s');
    //   sessionStorage.removeItem('_cux_pv');
    //   sessionStorage.removeItem('_cux_pv_ttl');
    // });

    const scriptTag = {
      content: getTrackingScript(this.config.production, this.config.project.slug),
    };

    await page.waitForSelector('head');
    await page.addScriptTag(scriptTag);
  };
}

// Change URL to website you want to test and slug for project on your local env
const artemis = new Artemis({
  production: false,
  project: {
    slug: '1428112091.0.06456300.1480349278.583c565e0fc5d',
    url: 'https://example.pl',
  },
});

artemis.openBrowser();

/**
 * This part is for executing a performance reports using lighthouse
 * Change URL, and slug
 * Then it will save a file called lhreport.html in your root with full report
 */

// const chromeLauncher = require('chrome-launcher');
// const puppeteer = require('puppeteer');
// const lighthouse = require('lighthouse');
// const request = require('request');
// const util = require('util');
// const fs = require('fs');
//
// (async() => {
//   const URL = 'https://www.example.pl/';
//
//   const opts = {
//     chromeFlags: ['--disable-web-security', '--allow-running-insecure-content', '--ignore-certificate-errors'],
//     logLevel: 'info',
//     output: 'json',
//     emulatedFormFactor: 'desktop',
//     ignoreHTTPSErrors: true,
//     throttling: {
//       rttMs: 40,
//       throughputKbps: 10 * 1024,
//       cpuSlowdownMultiplier: 1,
//       requestLatencyMs: 0, // 0 means unset
//       downloadThroughputKbps: 0,
//       uploadThroughputKbps: 0,
//     },
//   };
//
// // Launch chrome using chrome-launcher.
//   const chrome = await chromeLauncher.launch(opts);
//   // @ts-ignore
//   opts.port = chrome.port;
//
// // Connect to it using puppeteer.connect().
//   // @ts-ignore
//   const resp = await util.promisify(request)(`http://localhost:${opts.port}/json/version`);
//   const {webSocketDebuggerUrl} = JSON.parse(resp.body);
//   const browser = await puppeteer.connect({browserWSEndpoint: webSocketDebuggerUrl});
//
//   // @ts-ignore
//   browser.on('targetchanged', async target => {
//     const page = await target.page();
//     if (page && page.url() === URL) {
//       await page.addScriptTag({
//         content: getTrackingScript(false, '1981340103.0.79755100.1545381890.5c1ca802c2c94'),
//         // content: `(function(h,o,t,j,a,r){
//         // h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
//         // h._hjSettings={hjid:2060817,hjsv:6};
//         // a=o.getElementsByTagName('head')[0];
//         // r=o.createElement('script');r.async=1;
//         // r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
//         // a.appendChild(r);
//     // })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');`
//       });
//     }
//   });
//
//   const runnerResult = await lighthouse(URL, opts);
//   const reportHtml = runnerResult.report;
//   fs.writeFileSync('lhreport.html', reportHtml);
//
//   console.log('Report is done for', runnerResult.lhr.finalUrl);
//   console.log('Performance score was', runnerResult.lhr.categories.performance.score * 100);
//
//   await browser.disconnect();
//   await chrome.kill();
// })();
