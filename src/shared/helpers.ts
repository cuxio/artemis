const getTrackingDomain = (production: boolean): string =>
  production
    ? '//dc.cux.io/analyzer.js'
    : '//dc.cux.lo/analyzer.js';

export const getTrackingScript = (production: boolean, projectSlug: string): string => `
  (function(c,u,x,i,o){
    c._cuxSettings=c._cuxSettings||{id:o};
    var e=u.createElement(x),t=u.getElementsByTagName(x)[0];
    e.src=i;e.async=true;t.parentNode.insertBefore(e,t);
    c._cux_q=[];c._cux=c._cux||{send:function(n,p){c._cux_q.push({n:n,p:p,t:new Date()})}};
  })(window, document, 'script', '${getTrackingDomain(production)}', '${projectSlug}');
`;
