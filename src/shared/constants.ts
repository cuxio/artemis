import { BrowserOptionArgument } from './interfaces';
import { LaunchOptions } from 'puppeteer';

export const browserOptions: LaunchOptions = {
  args: [
    BrowserOptionArgument.DISABLE_WEB_SECURITY,
    BrowserOptionArgument.ALLOW_RUNNING_INSECURE_CONTENT,
  ],
  devtools: true,
  headless: false,
  defaultViewport: null,
  ignoreHTTPSErrors: true,
};
