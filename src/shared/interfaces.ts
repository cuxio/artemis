export interface Config {
  production: boolean;
  project: {
    slug: string;
    url: string;
  }
}

export enum BrowserOptionArgument {
  DISABLE_WEB_SECURITY = '--disable-web-security',
  ALLOW_RUNNING_INSECURE_CONTENT = '--allow-running-insecure-content',
}
