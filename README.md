# Artemis

## Usage

```
import { Artemis } from '@cux/artemis';

const artemis = new Artemis({
  production: false,
  replace: true,
  project: {
    slug: 'YOUR_PROJECT_SLUG',
    url: 'YOUR_PROJECT_URL'
  },
});

artemis.openBrowser();

```
